# dockerfiles

[![pipeline status](https://gitlab.com/nmeisenzahl/dockerfiles/badges/master/pipeline.svg)](https://gitlab.com/nmeisenzahl/dockerfiles/commits/master)

## About this Repo

This Repo contains some of my most used Dockerfiles as well as images. Nothing fancy. Why I use my own images instead any other available? Because they are ...

### Secure
My Dockerfiles containing the [Aqua Microscanner](https://github.com/aquasecurity/microscanner) which scans the images for package vulnerabilities and will skip the build process if needed.

### up-to-date

All images will be built automatically weekly (Sunday 1am CET) to ensure updated packages. My images are based on [alpine:latest](https://hub.docker.com/_/alpine/). The installed packages are always installed in the most recent version. 

__Feel free to use them without any warranty.__

More details: https://medium.com/01001101/some-basic-docker-images-9d3943e7b934

## About the Images

The images are stored in the Registry of this Repo. So far I only build images with latest tag which is overwritten weekly. This might change in the future.

Until now this Repo only provides some basic images which I mostly use in my containerized build pipelines. 

### curl

An image based on Alpine serving curl as entry point.

```
docker pull registry.gitlab.com/nmeisenzahl/dockerfiles/curl
docker run --rm registry.gitlab.com/nmeisenzahl/dockerfiles/curl "http://google.de"
```

### yamllint

An image based on Ubuntu serving yamllint without an entrypoint.

### kubectl

An image based on my curl image serving kubectl as entry point. You will need to mount your kubectl configuration to use it with your cluster. I'm using this image with Gitab CI, therefore, I skipped any further configuration options/envs.

```
docker pull registry.gitlab.com/nmeisenzahl/dockerfiles/kubectl
docker run --rm registry.gitlab.com/nmeisenzahl/dockerfiles/kubectl get pods
```

### helm

An image based on my kubectl image serving helm as entry point. 

```
docker pull registry.gitlab.com/nmeisenzahl/dockerfiles/helm
docker run --rm registry.gitlab.com/nmeisenzahl/dockerfiles/helm list
```

### helm-az

An image based on my kubectl image serving helm as entry point. Also includes the Azure CLI.

```
docker pull registry.gitlab.com/nmeisenzahl/dockerfiles/helm-az
docker run --rm registry.gitlab.com/nmeisenzahl/dockerfiles/helm-az list
```

### ansible

An image based on my curl image serving ansible-playbook as entry point. 

```
docker pull registry.gitlab.com/nmeisenzahl/dockerfiles/ansible
docker run --rm registry.gitlab.com/nmeisenzahl/dockerfiles/ansible --syntax-check
```

### terraform

An image based on my curl image serving terraform as entry point. 

```
docker pull registry.gitlab.com/nmeisenzahl/dockerfiles/terraform
docker run --rm registry.gitlab.com/nmeisenzahl/dockerfiles/terraform init
```

### terraform-az

An image based on my curl image serving terraform as entry point. Also includes the Azure CLI.

```
docker pull registry.gitlab.com/nmeisenzahl/dockerfiles/terraform-az
docker run --rm registry.gitlab.com/nmeisenzahl/dockerfiles/terraform-az init
```
